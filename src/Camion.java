/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author stag
 */
public class Camion extends Vehicule {

    //Proprietes
    private int charge;

    //Getter et setter
    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        if (charge >= 0) {
            this.charge = charge;
        } else {
            System.err.println("Pas de valeurs negative pour la charge!");

        }
    }
    
    public void charger(int charge){  
        setCharge(charge);
    }
    // Surcharge de la methode afficher
    public void afficher(){
        
        super.afficher();
        System.out.println("charge :" + charge);
    }

}
